package com.cosmindimoiu.github.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Repository(

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "full_name")
    val fullName: String?,

    @ColumnInfo(name = "description")
    val description: String?,

    @ColumnInfo(name = "avatar_url")
    val avatarUrl: String?,

    @ColumnInfo(name = "owner")
    val username: String?,

    @ColumnInfo(name = "forks")
    val forks: String?,

    @ColumnInfo(name = "watchers")
    val watchers: String?,

    @ColumnInfo(name = "stars")
    val stars: String?,

    @ColumnInfo(name = "url")
    val url: String?
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}