package com.cosmindimoiu.github.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface RepositoryDao {
    @Insert
    suspend fun insertAll(vararg repositories: Repository): List<Long>

    @Query("SELECT * FROM repository")
    suspend fun getAllRepositories(): List<Repository>

    @Query("SELECT * FROM repository WHERE id = :repositoryId")
    suspend fun getRepository(repositoryId: Int): Repository

    @Query("DELETE FROM repository")
    suspend fun deleteAllRepositories()
}