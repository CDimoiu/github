package com.cosmindimoiu.github.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.cosmindimoiu.github.R
import com.cosmindimoiu.github.viewmodel.ListViewModel
import com.cosmindimoiu.github.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment() {

    private lateinit var listViewModel: ListViewModel
    private lateinit var mainViewModel: MainViewModel
    private val repositoriesListAdapter = RepositoriesListAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.show()
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listViewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        listViewModel.refresh()

        repositoriesList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = repositoriesListAdapter
        }

        refreshLayout.setOnRefreshListener {
            repositoriesList.visibility = View.GONE
            listError.visibility = View.GONE
            loadingView.visibility = View.VISIBLE
            listViewModel.fetchFromDatabase()
            refreshLayout.isRefreshing = false
        }

        observeViewModel()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.run {
            mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        } ?: throw Throwable("invalid activity")
        mainViewModel.updateActionBarTitle("Android Repos")
    }

    private fun observeViewModel() {
        listViewModel.repositories.observe(viewLifecycleOwner, Observer { repositories ->
            repositories?.let {
                repositoriesList.visibility = View.VISIBLE
                repositoriesListAdapter.updateRepositoriesList(repositories)
            }
        })

        listViewModel.repositoriesLoadError.observe(viewLifecycleOwner, Observer { isError ->
            isError?.let {
                listError.visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        listViewModel.loading.observe(viewLifecycleOwner, Observer { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    repositoriesList.visibility = View.GONE
                }
            }
        })
    }
}
