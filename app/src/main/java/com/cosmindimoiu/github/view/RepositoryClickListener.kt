package com.cosmindimoiu.github.view

import android.view.View

interface RepositoryClickListener {
    fun onRepositoryClicked(v: View)
}