package com.cosmindimoiu.github.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.cosmindimoiu.github.R
import com.cosmindimoiu.github.database.Repository
import com.cosmindimoiu.github.databinding.ItemRepositoryBinding
import kotlinx.android.synthetic.main.item_repository.view.*
import java.util.*

class RepositoriesListAdapter(private val repositoriesList: ArrayList<Repository>) :
    RecyclerView.Adapter<RepositoriesListAdapter.RepositoryViewHolder>(), RepositoryClickListener {

    fun updateRepositoriesList(newRepositoriesList: List<Repository>) {
        repositoriesList.clear()
        repositoriesList.addAll(newRepositoriesList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val view = DataBindingUtil.inflate<ItemRepositoryBinding>(
            inflater,
            R.layout.item_repository,
            parent,
            false
        )
        return RepositoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.view.repository = repositoriesList[position]
        holder.view.listener = this
    }

    override fun getItemCount() = repositoriesList.size

    override fun onRepositoryClicked(v: View) {
        val id = v.repository_id.text.toString().toInt()
        val action = ListFragmentDirections.actionListFragmentToDetailFragment()
        action.repositoryId = id
        Navigation.findNavController(v).navigate(action)
    }

    class RepositoryViewHolder(var view: ItemRepositoryBinding) : RecyclerView.ViewHolder(view.root)
}