package com.cosmindimoiu.github.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cosmindimoiu.github.R
import com.cosmindimoiu.github.database.Repository
import com.cosmindimoiu.github.databinding.FragmentDetailBinding
import com.cosmindimoiu.github.viewmodel.DetailViewModel
import com.cosmindimoiu.github.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_detail.*


class DetailFragment : Fragment() {

    private lateinit var detailViewModel: DetailViewModel
    private lateinit var mainViewModel: MainViewModel
    private var repositoryId = 0

    private lateinit var dataBinding: FragmentDetailBinding
    private var repository: Repository? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            repositoryId = DetailFragmentArgs.fromBundle(it).repositoryId
        }

        detailViewModel = ViewModelProviders.of(this).get(DetailViewModel::class.java)
        detailViewModel.fetch(repositoryId)

        url.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(repository?.url)
            startActivity(intent)
        }

        observeViewModel()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.run {
            mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        } ?: throw Throwable("invalid activity")
        mainViewModel.updateActionBarTitle("Repository Details")
    }

    private fun observeViewModel() {
        detailViewModel.repositoryLiveData.observe(viewLifecycleOwner, Observer { repository ->
            this.repository = repository
            repository?.let {
                dataBinding.repository = repository
            }
        })
    }
}
