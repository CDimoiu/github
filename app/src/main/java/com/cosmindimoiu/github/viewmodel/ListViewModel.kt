package com.cosmindimoiu.github.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cosmindimoiu.github.database.Repository
import com.cosmindimoiu.github.database.RepositoryDatabase
import com.cosmindimoiu.github.networking.GitHubApiService
import com.cosmindimoiu.github.networking.GitHubResponse
import com.cosmindimoiu.github.util.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch

class ListViewModel(application: Application) : BaseViewModel(application) {

    private var prefHelper = SharedPreferencesHelper(getApplication())
    private var refreshTime = 15 * 60 * 1000 * 1000 * 1000L // the refresh happens every 15 minutes

    private val repositoriesService = GitHubApiService()
    private val disposable = CompositeDisposable()

    private val _repositories = MutableLiveData<List<Repository>>()
    val repositories: LiveData<List<Repository>> = _repositories

    private val _repositoriesLoadError = MutableLiveData<Boolean>()
    val repositoriesLoadError: LiveData<Boolean> = _repositoriesLoadError

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> = _loading

    fun refresh() {
        checkCacheDuration()
        val updateTime = prefHelper.getUpdateTime()
        if (updateTime != null && updateTime != 0L && System.nanoTime() - updateTime < refreshTime) {
            fetchFromDatabase()
        } else {
            fetchFromRemote()
        }
    }

    private fun checkCacheDuration() {
        val cachePreference = prefHelper.getCacheDuration()

        try {
            val cachePreferenceInt = cachePreference?.toInt() ?: 15 * 60
            refreshTime = cachePreferenceInt.times(1000 * 1000 * 1000L)
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        }
    }

    fun fetchFromDatabase() {
        _loading.value = true
        launch {
            val repositories =
                RepositoryDatabase(getApplication()).repositoryDao().getAllRepositories()
            repositoriesRetrieved(repositories)
        }
    }

    private fun fetchFromRemote() {
        _loading.value = true
        disposable.add(
            repositoriesService.searchAndroidRepos()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<GitHubResponse>() {

                    override fun onSuccess(response: GitHubResponse) {
                        val repositories = ArrayList<Repository>()
                        response.repositoryItems?.let { repositoryItems ->
                            for (repositoryItem in repositoryItems) {
                                val repository = Repository(
                                    repositoryItem.name,
                                    repositoryItem.fullName,
                                    repositoryItem.description,
                                    repositoryItem.owner?.avatarUrl,
                                    repositoryItem.owner?.username,
                                    repositoryItem.forks,
                                    repositoryItem.watchers,
                                    repositoryItem.stars,
                                    repositoryItem.url
                                )

                                repositories.add(repository)
                            }
                        }

                        storeRepositoriesLocally(repositories)
                    }

                    override fun onError(e: Throwable) {
                        _repositoriesLoadError.value = true
                        _loading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }

    private fun repositoriesRetrieved(repositoriesList: List<Repository>) {
        _repositories.value = repositoriesList
        _repositoriesLoadError.value = false
        _loading.value = false
    }

    private fun storeRepositoriesLocally(list: List<Repository>) {
        launch {
            val dao = RepositoryDatabase(getApplication()).repositoryDao()
            dao.deleteAllRepositories()
            val result = dao.insertAll(*list.toTypedArray())
            var i = 0
            while (i < list.size) {
                list[i].id = result[i].toInt()
                ++i
            }
            repositoriesRetrieved(list)
        }
        prefHelper.saveUpdateTime(System.nanoTime())
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}