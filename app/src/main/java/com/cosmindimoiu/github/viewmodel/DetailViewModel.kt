package com.cosmindimoiu.github.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cosmindimoiu.github.database.Repository
import com.cosmindimoiu.github.database.RepositoryDatabase
import kotlinx.coroutines.launch

class DetailViewModel(application: Application) : BaseViewModel(application) {

    private val _repositoryLiveData = MutableLiveData<Repository>()
    val repositoryLiveData: LiveData<Repository> = _repositoryLiveData

    fun fetch(id: Int) {
        launch {
            val repository = RepositoryDatabase(getApplication()).repositoryDao().getRepository(id)
            _repositoryLiveData.value = repository
        }
    }
}