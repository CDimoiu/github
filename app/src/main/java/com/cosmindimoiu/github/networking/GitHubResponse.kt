package com.cosmindimoiu.github.networking

import com.google.gson.annotations.SerializedName

class GitHubResponse {
    @SerializedName("items")
    val repositoryItems: List<RepositoryItem>? = null
}