package com.cosmindimoiu.github.networking

import com.google.gson.annotations.SerializedName

class RepositoryItem {

    @SerializedName("name")
    val name: String? = null

    @SerializedName("full_name")
    val fullName: String? = null

    @SerializedName("description")
    val description: String? = null

    @SerializedName("owner")
    val owner: Owner? = null

    @SerializedName("forks_count")
    val forks: String? = null

    @SerializedName("watchers_count")
    val watchers: String? = null

    @SerializedName("stargazers_count")
    val stars: String? = null

    @SerializedName("html_url")
    val url: String? = null
}