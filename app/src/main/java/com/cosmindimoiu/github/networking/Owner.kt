package com.cosmindimoiu.github.networking

import com.google.gson.annotations.SerializedName

class Owner {

    @SerializedName("login")
    val username: String? = null

    @SerializedName("avatar_url")
    val avatarUrl: String? = null
}