package com.cosmindimoiu.github.networking

import io.reactivex.Single
import retrofit2.http.GET

interface GitHubApi {
    @GET("/search/repositories?q=stars:%3E=10000+language:android&sort=stars&order=desc")
    fun searchAndroidRepos(): Single<GitHubResponse>
}